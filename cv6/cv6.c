#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void prehod(int *a, int *b)
{
	int p;
	p = *a;
	*a = *b;
	*b = p;
}

void prehod_ch(char *a, char *b)
{
        char p;
        p = *a;
        *a = *b;
        *b = p;
}


int main(int argc, char *argv[])
{
	int i;
	/*
	for (i = 0; i < argc; i++)
	{
			if (strcmp(argv[i], "-h") == 0)
			{
			printf("Nalezl jsem pozici/indexu %d\n", i);
				break;
			}
	}
	*/

	i= 0;
	while (i < argc)
	{
		if (strcmp(argv[i], "-h") == 0)
		{
                        printf("Nalezl jsem pozici/indexu %d\n", i);
                }
	i++;
	}

	// najít h v argumentech
	int a = 1;
	int b = 2;
	
	printf("a = %d b = %d\n",a,b);
	prehod(&a,&b);
	printf("a = %d b = %d\n",a,b);

	char ar[2][2] = {{'a','b'},{'a','b'}};
	printf("1 = %c 2 = %c\n",ar[0][1],ar[1][0]);
	prehod_ch(&(ar[0][1]),&(ar[1][0]));
	printf("a = %c b = %c\n",ar[0][1],ar[1][0]);
	
	// malloc pole 10 int;
	int delka = 10;
	int *pole = malloc(delka * sizeof(int));
	for (i = 0; i < delka; i++)
	{
		pole[i] = i;
	}
	for (i = 0; i < delka; i++)
	{
		printf("%d\n", pole[i]);
	}
	free(pole);
	return 0;
}
