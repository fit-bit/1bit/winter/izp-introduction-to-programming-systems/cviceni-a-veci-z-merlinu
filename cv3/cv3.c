#include <stdio.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
	//práce s textem
	char *str1 = "Hello";
	char *str2 = "world";

	printf("%s %s!\n", str1, str2);
	
	printf("1: %s\n2: %s\n", str1, str2);
	
	int i = 1;
	printf("%d: %s\n", i, str1);
	i = i + 1;
	printf("%d: %s\n", i, str2);

	// vypsání za hranice char


	// základní práce se souborem
	FILE *soubor;
	soubor = fopen("soubor.txt", "r");
	if (soubor != NULL)
	{
		char s[100];
		char s1[100];
		fscanf(soubor, "%99s %99s", s, s1);
		printf("První slovo souboru je '%s'\n", s);
		printf("Druhé slovo souboru je '%s'\n", s1);
		fclose(soubor);		

		//cyklus na slova
		soubor = fopen("soubor.txt", "r");
		int p = 0;
		char x[100];
		while (fscanf(soubor, "%99s", x) == 1)
		{
			p += 1;
			printf("%d. slovo je:' %s'\n", p, x);
		}
		printf("Počet slov v souboru je: %d\n", p);
		fclose(soubor);
	}

	// soubor pro zápis
	soubor = fopen("log.txt", "w");
	if (soubor != NULL)
	{
		fprintf(soubor, "Hello, world!\n");
		fclose(soubor);	
	}
	
	// cyklus na počet znaku v textu
        if (argc > 1)
     	{
		char c;
                char k;
                int m = 0;
         	soubor = fopen(argv[1], "r");
		if (soubor != NULL)
		{
			printf("Zadej znak: ");
			c = getchar();
			while ((k = fgetc(soubor)) != EOF )
			{
				if (tolower(k) == c)
				{
					m += 1;
				}
			}
		}
		printf("Soubor %s obsahuje %d výskyty znaku %c\n", argv[1], m, c);
        	fclose(soubor);


        }
	return 0;
}
