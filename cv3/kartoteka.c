#include <stdio.h>
#include <string.h>

int main ()
{
	FILE *folder;
	char name[50];
	char number[50];
	char place[50];
	printf("Kartotéka: program ukládá zadáná jména a rodná čísla pacientů do souborů pojmenovaných podle jmén a rodných čísel pacientů. (Program se ukončí zadáním nuly)\n"); // popis funkčnosti programu
	while (1)
	{	
		// načtení hodnot
		printf("Zadejte jmeno pacienta (ukoncene teckou): ");
		scanf("%s", name);
		printf("Zadejte rodne cislo: ");
		scanf("%s", number);
		printf("Zadejte bydliste (ukoncene teckou): ");
		scanf("%s", place);
		// ukončení zadávání
		if (name[0] == '0') break;
		else 
		{
			char file_name[150];
			strcat(file_name, name);
			strcat(file_name, number);
			strcat(file_name, ".txt");
			folder = fopen(file_name,"w");
			fprintf(folder,"Jmeno pacienta: %s\n", name);
			fprintf(folder,"Rodne cislo pacienta: %s\n", number);
			fprintf(folder,"Bydliste pacienta: %s\n", place);
			fclose(folder);
		}
	}
	printf("Pacienti uloženi\n");
	return 0;
}
