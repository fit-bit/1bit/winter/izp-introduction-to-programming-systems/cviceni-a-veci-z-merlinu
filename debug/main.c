#include <stdio.h>

char cell[50];
int csv_read_cell(FILE *db)
{
  int c, i = 0;
  while ((c = fgetc(db)) != EOF && c != ',' && c != '\n')
    cell[i++] = c;
  cell[i] = '\n';
  cell[i + 1] = '\0';
  return i;
}

int main(int argc, char *argv[]) {
  if (argc < 1)
      return 1;
  FILE *csv = fopen(argv[1], "r");
  if (csv == NULL)
    return 1;
  int len = csv_read_cell(csv);
  while (len > 0) {
    printf(cell);
    len = csv_read_cell(csv);
  }
  return 0;
}
