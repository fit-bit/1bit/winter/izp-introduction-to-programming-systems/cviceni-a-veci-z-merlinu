#include <stdio.h>

unsigned long long int i = 1;

void single_move(int odkud, int kam)
{
	printf("Krok = %llu %d -> %d\n", i++, odkud, kam);
}

void hanoi (int odkud, int kam, int pres, int kruhy)
{
  if (kruhy == 1)
  {
  	single_move(odkud, kam);
  }
  else
  {
   hanoi(odkud, pres, kam, kruhy-1);
  
   single_move(odkud, kam);
  
   hanoi(pres, kam, odkud, kruhy-1);
  }
}
 
int main()
{
	hanoi(1,2,3,25);
	return 0;
} 
