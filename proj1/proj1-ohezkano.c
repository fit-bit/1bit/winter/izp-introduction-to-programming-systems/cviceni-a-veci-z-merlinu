/******************************************************/
/* * *          Projekt 1 - Práce s textem        * * */
/* * *                                            * * */
/* * *                  Verze:1                   * * */
/* * *                                            * * */
/* * *               Matěj Kudera                 * * */
/* * *                říjen 2018                  * * */
/******************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

/**
 * Constanty které určují s jakou velikostí textu, příkazu a erroru můžeme pracovat.
 */
const unsigned t_lenght = 1500;
const unsigned i_lenght = 500;
const unsigned err_length = 300;

/**
 * Text help.
 */
const char help_text[] = 
	"Jednoduchý editor textu.\n"
	"\n"
	"Autor: Matěj Kudera\n"
	"\n"
	"Popis:\n"
	"Program pracuje s jedním argumentem a to se souborem s příkazy pro editaci textu.\n"
	"Text který se má upravovat se píše do standartního vstupu a následně je pak upravován podle příkazů v souboru.\n"
	"Upravený text je vypysován do standartního výstupu.\n"
	"\n"
	"Příkazy:\n"
	"--iCONTENT (insert) vloží před aktuální řádek řádek s obsahem CONTENT.\n"
	"--bCONTENT (before) vloží na začátek aktuálního řádku obsah CONTENT.\n"
	"--aCONTENT (after/append) vloží na konec aktuálního řádku obsah CONTENT.\n"
	"--d (delete) smaže aktuální řádek.\n"
	"--dN smaže N řádků (N > 0).\n"
	"--r (remove EOL) na aktuálním řádku odstraní znak konce řádku.\n"
	"--s PATTERN REPLACEMENT (substitute) na aktuálním řádku provede substituci (nahrazení) výskytu řetězce PATTERN za řetězec REPLACEMENT.\n"
	"  PATTERN musí být neprázdný. Pokud aktální řádek neobsahuje podřetězec PATTERN, příkaz nic nedělá.\n"
	"--s:PATTERN:REPLACEMENT substituce řetězce, přičemž PATTERN a REPLACEMENT mohou být od sebe oddělené libovolným tisknutelným znakem,\n"
	"  zde vyjádřeno znakem :. PATTERN v takovém případě nesmí obsahovat daný oddělovač.\n"
	"--S:PATTERN:REPLACEMENT stejné jako s:PATTERN:REPLACEMENT, ale nahrazuje všechny výskyty PATTERN.\n"
	"--n (next) posun aktuálního řádku o jeden níž, tj. tiskne aktuální řádek.nN posun aktuálního řádku o N níž. (N > 0).\n"
	"--q (quit) ukončí editaci, další řádky nezpracovává a ani netiskne.\n"
	"--gX (goto) přeskoč s aktuálním příkazem pro editaci na X-tý příkaz.\n"
	"  X vyjadřuje číslo řádku (příkazu) v příkazovém souboru (1 <= X <= počtu řádků příkazového souboru).\n"
	"--fPATTERN (find) posun aktuálního řádku směrem dolů na řádek obsahující řetězec PATTERN.\n"
	"--cX PATTERN (conditioned goto) přeskoč s aktuálním příkazem pro editaci na X-tý příkaz v případě,\n"
	"  že aktuální řádek obsahuje řetězec PATTERN. Pokud aktuální řádek řetězec neobsahuje, pokračuj s následujícím příkazem.\n"
	"--e (append EOL) přidej na konec aktuálního řádku znak konce řádku.";

/**
 *
 * Když se končí program, zavolá se tato funkce a zkontroluje jestli poslední string co byl vytisknutý má \n na konci.
 *
 * @param printed: String který byl poslední vytisknutý na stdout.
 */
void check_n(char printed[t_lenght])
{
        if (strcmp(printed, "") != 0)
        {
                if (printed[strlen(printed) - 1] != '\n')
                {
                        fprintf(stdout, "\n");
                }
        }
}

/**
 * Funkce zavírá soubor s příkazy a otestuje správné zavření.
 *
 * @param *instructions: Soubor ve kterém jsou příkazy.
 * @param printed: String který byl poslední vytisknutý na stdout.
 */
void close_file(FILE *instructions, char printed[t_lenght])
{
        if (fclose(instructions) == EOF)
        {
		check_n(printed);
                fprintf(stderr, "Nepodařilo se uzavřít soubor s příkazy.\n");
		exit(1);
        }

}

/**
 * Funkce kontroluje jestli ve stringu instrukce (která je zadaná od druhého znaku) jsou zadaná jen čísla.
 *
 * @param instruction: Text instrukce načtené ze souboru.
 * @return: Logickou hodnotu 1 když to jsou jen čísla, jinak 0.
 */
int only_numbers(char instruction[i_lenght])
{
	int i = 1;

	while (instruction[i] != '\n')
        {
       		if(!isdigit(instruction[i]))
        	{
        		return 0;
       		}
      		i++;
        }

	return 1;
}

/**
 * Funkce poskládá číslo ze zadaného stringu. Jestli je string jen číslice se kontroluje v only_numbers.
 *
 * @param instruction: Text ze kterého chceme poskládád číslo.
 * @return: Vrací poskládané číslo v int podobě.
 */
int make_number(char instruction[i_lenght])
{
	int len = 0;
	int dec = 0;
	len = strlen(&instruction[1]);
        for(int i = 1; i < len; i++)
        {
        	dec = dec * 10 + (instruction[i] - '0');
        }

	return dec;
}

/**
 * Funkce kontroluje, jestli je text pro úpravu načtený v proměnné text (text je předávaný odkazem).
 * Když ne, tak načte text do proměnné ze standartního výstupu.
 *
 * @param *text: Odkaz na proměnnou text, ve které je text pro editaci.
 * @param *instructions: Soubor ve kterém jsou příkazi pro editaci.
 * @param printed: Proměnná ve které je uložené co se naposled vypsalo do stdin.
 * Potřebné pro kontrolu \n na konci posledního řádku.
 */
void check_text(char *text, FILE *instructions, char printed[t_lenght])
{
	if (strcmp(text, "") == 0)
        {
        	if(!fgets(text, t_lenght, stdin))
                {
			check_n(printed);
                	close_file(instructions, printed);
                        exit(0);
                }
        }
}

/**
 * Funkce se volá vždy když v programu dojde k chybě.
 * Funkce vypíše chybové hlášení a ukončí program.
 *
 * @param *instructions: Soubor ve kterém jsou uloženy příkazi pro editaci textu.
 * @param er_text: Text který chceme vypsat jako error.
 * @param printed: Proměnná ve které je uložené co se naposled vypsalo do stdin.
 */
void err_and_quit(FILE *instructions, char er_text[err_length], char printed[t_lenght])
{
	check_n(printed);
	fprintf(stderr, er_text);
        close_file(instructions, printed);
        exit(1);
}

/**
 * Funkce kontoluje jestli je zadané číslo 0. Používá se v příkazech 'd', 'n' a 'g',
 * protože podle zadání nemůže být smazání 0 řádků, posun o 0 řádků nebo posun na nultý příkaz.
 *
 * @param dec: Číslo které se kontroluje.
 * @param *instructions: Soubor ve kterém jsou uloženy příkazi pro editaci textu.
 * @param er_text: Když je číslo 0 použije se tento text jako error výpis.
 * @param printed: Proměnná ve které je uložené co se naposled vypsalo do stdin.
 */
void check_null(int dec, FILE *instructions, char er_text[err_length], char printed[t_lenght])
{
        if(dec == 0)
        {
                err_and_quit(instructions, er_text, printed);
        }
}

/**
 * Funkce otevýrá soubor s příkazy, a když se nepovede otevřít vypíše chybu a ukončí program.
 *
 * @param *instructions: Soubor ve kterém jsou uloženy příkazi pro editaci textu.
 * @param *argv: Odkaz na pole ve kterém jsou argumenty programu.
 */
void open_instr(FILE *instructions, char *argv[])
{
	if ((instructions = fopen(argv[1], "r")) == NULL)
        {
                fprintf(stderr, "Nepodařilo se otevřít soubor s příkazy.\n");
                exit(1);
        }
}

/**
 * Příkaz i. Vloží před aktuální řádek řádek s obsahem CONTENT.
 * 
 * @param *instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void i(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	check_text(text, instructions, printed);

	//Kontrola jestli je za příkazem napsáno ce se má vypsat. Jinak error.
	if (instruction[1] == '\n')
        {
		err_and_quit(instructions, "Špatně zadaný příkaz i. Není zadáno co vypsat.\n", printed);
        }
        else
        {
        	fprintf(stdout, "%s", &instruction[1]);
        }
}

/**
 * Příkaz b. Vloží na začátek aktuálního řádku obsah CONTENT.
 * 
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void b(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	check_text(text, instructions, printed);
				
	//Kontrola jestli je za příkazem napsáno ce se má vypsat. Jinak error.
        if (instruction[1] == '\n')
        {
		err_and_quit(instructions, "Špatně zadaný příkaz b. Není zadáno co dát na začátek.\n", printed);
        }
        else
        {
		// Kontrola jestli po přidání textu není výsledný text delší než námi stanovený limit.
        	if ((strlen(text) + strlen(instruction) - 1) > t_lenght)
                {
			err_and_quit(instructions, "Délka řádku + doplnění na začátek je delší než maximální délka řádku.\n", printed);
                }
                else
                {
			// Poskládání výsledného textu, pomocí proměnné str.
                        char str[t_lenght];
			str[0] = '\0';

                       	instruction[strlen(instruction) - 1]= 0;
                        strcat(str, &instruction[1]);
                        strcat(str, text);
                        strcpy(text, str);
                }
        }
}

/**
 * Příkaz a. Vloží na konec aktuálního řádku obsah CONTENT.
 * 
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void a(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	check_text(text, instructions, printed);

	//Kontrola jestli je za příkazem napsáno ce se má vypsat. Jinak error.
        if (instruction[1] == '\n')
	{
		err_and_quit(instructions, "Špatně zadaný příkaz a. Není zadáno co dát na konec.\n", printed);
	}
	else
	{
		// Kontrola jestli po přidání textu není výsledný text delší než námi stanovený limit.
		if ((strlen(text) + strlen(instruction) - 1) > t_lenght)
		{
			err_and_quit(instructions, "Délka řádku + doplnění na konec je delší než maximální délka řádku.\n", printed);
		}
		else
		{
			// Předtím než se složí vysledný text se musí odstranit znak konce řádku ze stringu text.
			// Text který k tomu přídáváme má vlastní konec řádku.
			if (text[strlen(text) - 1] == '\n')
			{
				text[strlen(text) - 1] = 0;
			}

			strcat(text, &instruction[1]);
		}
	}
}

/**
 * Příkaz d a dN. Příkaz d smaže 1 řádek, příkaz dN smaže N řádků. Příčemž N nesmá být záporné a nula.
 * 
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void d(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Rozdělení podle délky řetězce na d a dN. 'd' je osamocené a dn má číslo.
	if (strlen(instruction) > 2)
	{
		int i, ok;
		i = 1;

		// Kontrola jestli jsou za příkazem napsány jen číslice.
		ok = only_numbers(instruction);
	
		// Pokud příkaz neobsahuje jen číslice vypíše se chyba.
		if (ok == 1)
		{
			int dec = 0;
			dec = make_number(instruction);
	
			// Kontrola jestli není číslo 0, nejde vymyzat 0 řádků.
			check_null(dec, instructions, "Špatně zadaný příkaz dN, nejde smazat 0 řádků.\n", printed);
			check_text(text, instructions, printed);

			// Vymazání zadaného počtu řádků. Mazání se provádí tím že se přeskočí.
			for (i = 0; i < dec; i++)
			{
				// Kdyby došly řádky program se ukončí.
				if(!fgets(text, t_lenght, stdin))
				{
					check_n(printed);
					close_file(instructions, printed);
					exit(0);
				}
			}

		}
		else
		{
			err_and_quit(instructions, "Špatně zadaný příkaz gN, neobsahuje jen číslice.\n", printed);
		}
	}
	//Příkaz d.
	else
	{	
		check_text(text, instructions, printed);
	
		// Vymazání řádku. Kdyby za ním už žádný řádek nebyl program se ukončí.
		if (!fgets(text, t_lenght, stdin))
		{
			check_n(printed);
			close_file(instructions, printed);
			exit(0);
		}
	}
}

/**
 * Příkaz r. Odstraní znak konce řádku na aktuálním řádku.
 * 
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void r(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Ověření jestli je příkaz zadaný správně. Příkaz má být zadaný samostatně.
	if (strlen(instruction) > 2)
	{
		err_and_quit(instructions, "Špatně zadaný příkaz r, příkaz má být samostatný.\n", printed);
	}
	else
	{
		check_text(text, instructions, printed);
		
		// Kontrola jestli už náhodou není znak konce řádku smazaný.
		if (text[strlen(text) - 1] == '\n')
		{
			text[strlen(text) - 1] = '\0';
		}
	}
}

/**
 * Příkazy s a S. Příkaz s nahradí zadaný string pattern za string replacement v textu.
 * Příkaz S udělá to stejné co s, ale nahradí všechny výskity na řádku.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void s(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Kontrola jestli je instrukce tak dlouhá aby mohla obsahovat nejmenší možný příkaz s nebo S, tak aby fungoval.
	if (strlen(instruction) > 4)
	{
		// Do proměnné pattern se poskládá string který se má najít a nahradit
		// Do proměnné replacement se poskládá string který nahradí pattern.
		// V proměnné separator je uložen char který zadal uživatel jako rozdělení pattern a replacement.
		// I je aktuální pazice v řádku příkazu. Používá se pro najití pattern a replacement na řádku příkazu.
		char separator;
		char pattern[i_lenght];
		char replacement[i_lenght];
		int i;

		pattern[0] = '\0';
		replacement[0] = '\0';
		i = 2;

		// Když na druhé a třetí pozici v příkazu budu zedáno to stejné, neboli oddělovač, string neobsahuje pattern.
		if (instruction[1] == instruction[2])
		{
			err_and_quit(instructions, "Špatně zadaný některý příkaz s, příkaz neobsahuje pattern.\n", printed);
		}

		// Kontrola jestli je separator tisknutelný znak.
		if (iscntrl(instruction[1]))
		{
			err_and_quit(instructions, "Špatně zadaný některý příkaz s, separator není tisknutelný znak.\n", printed);
		}
	
		// Do separator se uloží char který si vybral uživatel jako oddělovač.
		separator = instruction[1];

		check_text(text, instructions, printed);

		// Poskládání pattern
		do
		{
			char p[2] = "";
			p[0] = instruction[i];
			p[1] = '\0';
			strcat(pattern, p);
			i++;

			// Kdyby jsme náhodou nenašly druhý oddělovač a budume na konci textu.
			// Tak příkaz je špatně zadaný, neobsahuje replacement.
			if ((unsigned)i == strlen(instruction) - 2)
			{
				break;
			}
		} while (instruction[i] != separator);

		// Jestli po pattern najdeme separator ale jsme na konci příkazu.
		// Příkaz je zase špatně zadaný, neobsahuje Replacement
		if (instruction[i] != separator && (unsigned)i == strlen(instruction) - 2)
		{
			err_and_quit(instructions, "Špatně zadaný některý příkaz s, příkaz potřebuje parrametry pattern a replacement.\n", printed);
		}
	
		// Když ještě za separatorem něco je, poskládá se text replacement.
		if ((unsigned)i != strlen(instruction) - 2)
		{
			i++;

			do
			{
				char p[2]="";
				p[0] = instruction[i];
				p[1] = '\0';
				strcat(replacement, p);
				i++;
			} while ((unsigned)i != strlen(instruction) - 1);
		}

		// V proměnné dec je uložena pozice pattern, když se nenajde tak nic.
		// Stop se používá jako logická proměnná pro ukončení cyklu nahrazování v textu.
		// Start je ukazatel na začátek pattern v tektu, když se nenajde tak nic,
		int dec = 0;
		int stop = 0;
		char *start;

		// Cyklus pro nahrazování pattern v textu. Výckrát se spustí jen u příkazu S.
		while (stop == 0)
		{
			// Když najde pattern, vrátí nám ukazatel na začátek pattern v textu.
			if((start = strstr(&text[dec], pattern)) != NULL)
			{
				// Kontrola jestli nahrazení textu nepřekročí námi stanovený limit délky textu.
				if (strlen(text) - strlen(pattern) + strlen(replacement) > t_lenght)
				{	
					err_and_quit(instructions, "Nelze vyměnit, výsledný text je moc dlouhý\n", printed);
				}
	
				// Do proměnné dec se uloží jak daleko je pattern v textu.
				dec = 0;

				while (&text[dec] != start)
				{
					dec++;
				}
	
				// Poskládání výsledného řetězce.
				char str[t_lenght];
				str[0] = '\0';

				for ( i = 0; i != dec; i++)
				{
					char p[2] = "";
					p[0] = text[i];
					p[1] = '\0';
					strcat(str, p);
				}

				strcat(str, replacement);
				strcat(str, &(text[i + strlen(pattern)]));
				strcpy(text, str);
				memset(str, 0, t_lenght);

				// Zjistí jestli se jedná o příkaz S nebo s, když s ukončí cyklus.
				if (instruction[0] == 's')
				{
					stop = 1;
				}
			}
			// Ukončení cyklu když se pattern v textu nenajde.
			else
			{
				stop = 1;
			}
	
			// Kdyby z nějakého důvodu počítadlo dec bylo větší než délka textu, tak se cyklus ukončí.
			if ((unsigned)dec == strlen(text))
			{
				break;
			}

			dec++;
		}
	}
	else
	{
		err_and_quit(instructions, "Špatně zadaný některý příkaz s, příkaz potřebuje parrametry pattern a replacement.\n", printed);
	}
}

/**
 * Příkazy n a nN. Příkaz n vypíše jeden řádek tetxu na standartní výstup. nN vypáše N řádků.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void n(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Zjištění jestli se jedná o příkaz n neno nN.
	// Řeší se to podle velikosti příkazu. Příkaz n je napsaný sám a příkaz nN má u sebečíslo.
	if (strlen(instruction) > 2)
	{
		// Ovšření jestli v příkazu jsou jen číslice.
		int i, ok;
		i = 1;
		ok = only_numbers(instruction);
	
		// Pokud za příkazem nejsou jen číslice, vypíše se chyba.
		if (ok == 1)
		{
			// Poskládání čísla z příkazu a ověření jestli číslo není 0, protože nejde vypsat 0 řádků. 
			int dec = 0;
			dec = make_number(instruction);
			check_null(dec, instructions, "Špatně zadaný příkaz nN, nejde se posunout o 0 řádků.\n", printed);

			// Podívá se jestli už je nějaký text načtený, když ano, vypíše a sníží počet vypsáni v cylku o 1.
			if (strcmp(text, "") != 0)
			{
				strcpy(printed, text);
				dec = dec - 1;
				fprintf(stdout, text);
				memset(text, 0, t_lenght);
			}
	
			// Vypsání N řádků. Kdyby došly řádky na výpis program skončí.
			for (i = 0; i < dec; i++)
			{
				if(fgets(text, t_lenght, stdin))
				{
					strcpy(printed, text);
					fprintf(stdout, text);
					memset(text, 0, t_lenght);
				}
				else
				{
					close_file(instructions, printed);
					exit(0);
				}
			}
		}
		else
		{
			err_and_quit(instructions, "Špatně zadaný příkaz nN, neobsahuje jen číslice.\n", printed);
		}
	}
	// Příkaz n.
	else
	{
		// Podívá se jestli už je nějaký text načtený, a jestli ano, tak ho vytiskne.
		if (strcmp(text, "") != 0)
		{
			strcpy(printed, text);
			fprintf(stdout, text);
			memset(text, 0, t_lenght);
		}
		// Když ještě text nemáme načtený, tak se načte a rovnou vytiskne.
		// Kdyby už na vstupu nebyl text program se ukončí.
		else
		{
			if(fgets(text, t_lenght, stdin))
			{
				strcpy(printed, text);
				fprintf(stdout, text);
				memset(text, 0, t_lenght);
			}
			else
			{
				close_file(instructions, printed);
				exit(0);
			}
		}
	}
}

/**
 * Příkaz q. Ukončení editaci textu.
 * Nevypíše žádný text a ukončí program.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void q(FILE *instructions, char printed[t_lenght], char instruction[i_lenght])
{
	// Ověření jestli je příkaz zadaný správně.
	// Příkaz má být zapsaný sám, nic k němu.
	if (strlen(instruction) > 2)
	{
		err_and_quit(instructions, "Špatně zadaný příkaz q, příkaz má být samostatný.\n", printed);
	}
	else
	{
		close_file(instructions, printed);
		exit(0);
	}
}

/**
 * Příkaz g. Přeskočí s aktuálním příkazem na X příkaz v souboru s příkazy.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *argv: Odkaz na pole ve kterém jsou argumenty programu.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void g(FILE *instructions, char *argv[], char printed[t_lenght], char instruction[i_lenght])
{
	// Ověření jestli je příkaz zadaný správně.
	// U příkazu musí být napsaný na jaký příkaz se má posunout.
	if (strlen(instruction) > 2)
	{
		int i, ok;
		i = 1;

		// Kontrola jestli jsou v příkazu zadány jen číslice.
		ok = only_numbers(instruction);

		// Pokud příkaz neobsahuje jen číslice vypíše se chyba.
		if (ok == 1)
		{
			// Vytvoření čísla z textu.
			int dec = 0;
			dec = make_number(instruction);

			// Kontrola jestli číslo není 0. Nejde se posunout na 0. příkaz.
			check_null(dec, instructions, "Špatně zadaný příkaz gX, nejde se posunout na 0. příkaz.\n", printed);
	
			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);

			// Vypočítání počtu řádků v souboru.
			int ch = 0;
			int lines = 0;

			while(!feof(instructions))
			{
				ch = fgetc(instructions);
				if(ch == '\n')
				{
					lines++;
				}
			}

			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);

			//Kontrola jestli číslo není stejné jako pozice příkazu g v souboru.
			//Kdyby ano, posun na stejný znak by znamenalo zacyklení.
			i = 0;
			int g = 0;
			char str[t_lenght];
			str[0] = '\0';

			while (fgets(str, sizeof(str), instructions))
			{
				i++;

				if (str[0] == 'g')
				{
					g = i;
					break;
				}
			}		

			if (g == dec)
			{
				err_and_quit(instructions, "Špatně zadaný příkaz gX, přesun na stejný příkaz spůsobý zacyklení.\n", printed);
			}

			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);
						
			// Kontrola jestli číslo, na který řádek se máme ponunout, není větší než celkový počet řádků.
			// To by znamenalo chybu.
			if (dec > lines)
			{
				err_and_quit(instructions, "Špatně zadaný příkaz gN, příkaz X v souboru není.\n", printed);
			}

			// Ponun na příkaz zadaný číslem.
			for (i = 1; i < dec; i++)
			{
				fgets(instruction, i_lenght, instructions);
			}
		}
		else
		{
			err_and_quit(instructions, "Špatně zadaný příkaz gX, neobsahuje jen číslice.\n", printed);
		}
	}
	else
	{
		err_and_quit(instructions, "Špatně zadaný příkaz gX, příkaz potřebuje zadané číslo.\n", printed);
	}
}

/**
 * Příkaz e. Doplní znak donce řádku na konec textu.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void e(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Ověření jestli je příkaz zadaný správně. Příkaz má být zadaný samostatně.
	if (strlen(instruction) > 2)
	{
		err_and_quit(instructions, "Špatně zadaný příkaz e, příkaz má být samostatný.\n", printed);
	}
	else
	{
		check_text(text, instructions, printed);

		// Kontrola jestli na konci textu je znak konce textu. Jestli ne, tak doblní.
		if (text[strlen(text) - 1] != '\n')
		{
			char p[2] = "";
			p[0] = '\n';
			p[1] = '\0';
			strcat(text, p);
		}
	}
}

/**
 * Příkaz f. Posun řádcku na řádek obsahující pattern.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 */
void f(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght])
{
	// Ověření jestli je příkaz zadaný správně. Příkaz má u sebe mát pattern.
	if (strlen(instruction) > 2)
	{
		check_text(text, instructions, printed);
		
		// Vymazání znaku konce řádku z instrukce;	
		instruction[strlen(instruction) - 1]= 0;

		// Procházení řádků dokud na řádku není pattern.
		while(strstr(text, &instruction[1]) == NULL)
		{
			strcpy(printed, text);
			fprintf(stdout, text);

			if(!fgets(text, t_lenght, stdin))
			{
				close_file(instructions, printed);
                		exit(0);
			}
		}
	}
	else
	{
		err_and_quit(instructions, "Špatně zadaný příkaz f, za příkazem musí být napsaný pattern.\n", printed);
	}
}

/**
 * Příkaz f. Posun řádcku na řádek obsahující pattern.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 * @param *argv: Odkaz na pole ve kterém jsou argumenty programu.
 */
void c(FILE *instructions, char *text, char printed[t_lenght], char instruction[i_lenght], char *argv[])
{
	// Kontrola jestli je instrukce tak dlouhá aby mohla obsahovat nejmenší možný příkaz c, tak aby fungoval.
	if (strlen(instruction) > 4)
	{
		int len = 0;
		int dec = 0;
		int i = 1;
		char pattern[i_lenght];
		pattern[0] = '\0';

		check_text(text, instructions, printed);

		// Vymazání znaku konce řádku z instrukce;	
		instruction[strlen(instruction) - 1]= 0;

		len = strlen(&instruction[1]);

		// Ovšření jestli je X číslo.
		while (instruction[i] != ' ')
        	{
			if (i == len)
			{
				err_and_quit(instructions, "Špatně zadaný příkaz c, příkaz neobsahuje pattern.\n", printed);
			}

       			if(!isdigit(instruction[i]))
        		{
				err_and_quit(instructions, "Špatně zadaný příkaz c, v části X nejsou jen čísla.\n", printed);
       			}
      			i++;
        	}
	
		// Vytvoření čísla z textu.
        	for (int j = 1; j < i; j++)
        	{
        		dec = dec * 10 + (instruction[j] - '0');
        	}

		// Kdyby za mezerou už nic nebylo, tak je příkaz zadaný špatně. Nemá pattern.
		if (i == len)
		{
			err_and_quit(instructions, "Špatně zadaný některý příkaz c, příkaz neobsahuje pattern.\n", printed);
		}
		
		// Poskládání patternu.
		i++;
		do
		{
			char p[2] = "";
			p[0] = instruction[i];
			p[1] = '\0';
			strcat(pattern, p);
			i++;
		} while (i != len + 1);
		
		// Kontrola jestli je na řádku pattern. Jentli ano, provede se skok.
		// Jestli ne, nic se neprovede.
		if (strstr(text, pattern) != NULL)
		{
			check_null(dec, instructions, "Špatně zadaný příkaz c, nejde se posunout na 0. příkaz.\n", printed);

			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);

			// Vypočítání počtu řádků v souboru.
			int ch = 0;
			int lines = 0;

			while(!feof(instructions))
			{
				ch = fgetc(instructions);
				if(ch == '\n')
				{
					lines++;
				}
			}

			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);

			//Kontrola jestli číslo není stejné jako pozice příkazu g v souboru.
			//Kdyby ano, posun na stejný znak by znamenalo zacyklení.
			i = 0;
			int g = 0;
			char str[t_lenght];
			str[0] = '\0';

			while (fgets(str, sizeof(str), instructions))
			{
				i++;

				if (str[0] == 'c')
				{
					g = i;
					break;
				}
			}		

			if (g == dec)
			{
				err_and_quit(instructions, "Špatně zadaný příkaz c, přesun na stejný příkaz spůsobý zacyklení.\n", printed);
			}

			// Zavření a znovu otevření souboru s příkazy aby jsme se dostaly na začátek.
			close_file(instructions, printed);
			open_instr(instructions, argv);
						
			// Kontrola jestli číslo, na který řádek se máme ponunout, není větší než celkový počet řádků.
			// To by znamenalo chybu.
			if (dec > lines)
			{
				err_and_quit(instructions, "Špatně zadaný příkaz c, příkaz X v souboru není.\n", printed);
			}

			// Ponun na příkaz zadný číslem.
			for (i = 1; i < dec; i++)
			{
				fgets(instruction, i_lenght, instructions);
			}
		}
	}
	else
	{
		err_and_quit(instructions, "Špatně zadaný příkaz c, příkaz potřebuje číslo a pattern.\n", printed);
	}
}

/**
 * Funkce se provede když je špatně zadaný příkaz v souboru s příkazy.
 * Funkce ukončí program a napíše která instrukce je špatná
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param instruction: Promenná ve které je uložena aktuálně načtenná instrukce.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 */
void bad_instruction(FILE *instructions, char instruction[i_lenght], char printed[t_lenght])
{
	// Proměnná do které se poskládá výsledný errorový text.
	char err_text[err_length];
	err_text[0] = '\0';

	char p[2] = "";
	p[0] = instruction[0];
	p[1] = '\0';
	strcat(err_text, "Špatný příkaz v souboru. Příkaz: '");
	strcat(err_text, p);
	strcat(err_text, "' není podpororovaný.\n");

	// Ukončení programu.
	err_and_quit(instructions, err_text, printed);
}

/**
 * Vypsání zbývajícího textu na standartní výstup když nám dojdou příkazy pro úpravu textu.
 *
 * @param instructions: Soubor s instrukcemi pro úpravu textu.
 * @param *text: Ukazatel na promennou text ve které je uložený text k editaci.
 * @param printed: Promenná ve které je uložen poslední text který se vytisknul.
 */
void print_rest(FILE *instructions, char *text, char printed[t_lenght])
{
	// Zjistí jestli náhodou už nějaký text nemáme načtený, když ano, tak ho vypíše.
	if (strcmp(text, "") != 0)
	{
		fprintf(stdout, text);
		strcpy(printed, text);
		memset(text, 0, t_lenght);                
	}

	// Vypisuje zbylé řádky dokud nedojdou.
	while (fgets(text, t_lenght, stdin))
	{
		strcpy(printed, text);
		fprintf(stdout, "%s", text);
		memset(text, 0, t_lenght);
	}

	close_file(instructions, printed);
}

/**
 * Ve funkce se vykonává úprava textu podle příkazů v souboru.
 * Funkce může skončit nějakým errorem během úpravy textu, nebo až provede celou úpravu textu.
 *
 * @param *argv: Odkaz na pole ve kterém jsou argumenty programu.
 */
void edit_text(char *argv[])
{
	// Otevření souboru s příkazy a ověření jestli se soubor otevřel.
	FILE *instructions;
	if ((instructions = fopen(argv[1], "r")) == NULL)
        {
                fprintf(stderr, "Nepodařilo se otevřít soubor s příkazy.\n");
                exit(1);
        }

	// V proměnné text je uložený řádek textu který upravujeme.
	// v proměnné instruction je uložený příkaz ze souboru pro práci s textem.
	// V proměnné printed je uložený posledný text který se vypsal na standartní výstup.
	char text[t_lenght];
        char instruction[i_lenght];
	char printed[t_lenght];

	// Vynulování proměnných;
	text[0] = '\0';
	instruction[0] = '\0';
	printed[0] = '\0';

	// Načtení příkazu do proměnné instruction ze souboru s instrukcemi. Příkaz se vykoná a cyklus našte další příkaz.
	// Když neukončíme cyklus zevnitř, standartně se ukončí na poslednám příkazu.
	while (fgets(instruction, sizeof(instruction), instructions))
        {
		// Kdyby byl v souboru prázdný řádek, tak se přeskočí.
		if (strlen(instruction) == 1)
		{
			continue;
		}

		// Zjistí o jaký příkaz se jedná a zavolá funkci pro daný příkaz. Když je v instrukcích nějaký nepodporovaný příkaz vypíše chybu.
		switch (tolower(instruction[0]))
		{
			case 'i':
				i(instructions, text, printed, instruction);
				break;

			case 'b':
				b(instructions, text, printed, instruction);
				break;
			
			case 'a':
				a(instructions, text, printed, instruction);
				break;
			
			case 'd':
				d(instructions, text, printed, instruction);
				break;

			case 'r':
				r(instructions, text, printed, instruction);
				break;
			
			case 's':
				s(instructions, text, printed, instruction);
				break;

			case 'n':
				n(instructions, text, printed, instruction);
				break;

			case 'q':
				q(instructions, printed, instruction);
                                break;
			
			case 'g':
				g(instructions, argv, printed, instruction);
				break;

			case 'f':
				f(instructions, text, printed, instruction);
				break;
			
			case 'c':
				c(instructions, text, printed, instruction, argv);
				break;

			case 'e':
				e(instructions, text, printed, instruction);
				break;
			
			default:
				bad_instruction(instructions, instruction, printed);
                                break;
		}
	}
	// vypsání zbylích řádků.
	print_rest(instructions, text, printed);
}

/**
 * Tělo programu.
 *
 * @param argc: Počet zadaných argumentů programu.
 * @param *argv: Ukazatel na pole zadaných argumentů.
 * @return: 0 jestli program proběhl správně, jinak 1.
 */
int main(int argc, char *argv[])
{
	// Ošetření možnosti nezadání argumentu názvu souboru s příkazy pro úpravu textu, nebo zadání více argumentů.
	if (argc != 2)
        {
		fprintf(stderr, help_text);
                return 1;
	}

	// Ve funkci se provádí úprava textu.
	edit_text(argv);
	
	// Když se program dostane sem, znamená to že nenastala žádná chyba a program úspěšně skončí.
	return 0;
}
