/******************************************************/ 
/* * *   Projekt 3: Jednoduchá shluková analýza   * * */ 
/* * *                                            * * */ 
/* * *                  Verze:1                   * * */ 
/* * *                                            * * */ 
/* * *               Matěj Kudera                 * * */ 
/* * *              prosinec 2018                 * * */ 
/******************************************************/ 

/**
 * Kostra programu pro 3. projekt IZP 2018/19
 *
 * Jednoducha shlukova analyza: 2D nejblizsi soused.
 * Single linkage
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX
#include <ctype.h> // isdigit

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *      #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - "__FILE__":%u: "s"\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *      pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */

/**
 *  struktura objektu: identifikator a souradnice.
 */
struct obj_t {
	int id;
    	float x;
    	float y;
};

/**
 * shluk objektu:
 *	pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */
struct cluster_t {
    	int size;
    	int capacity;
    	struct obj_t *obj;
};

// Help programu.
char *help = "Jednoduchá shluková analýza\n"
		"\n"
		"Autor: Matěj Kudera\n"
		"\n"
		"Program se spouští v následující podobě:\n"
		"./proj3 SOUBOR [N]\n"
		"\n"
		"Argumenty programu:\n"
		"-SOUBOR je jméno souboru se vstupními daty.\n"
		"-N je volitelný argument definující cílový počet shluků. N > 0. Výchozí hodnota (při absenci argumentu) je 1.\n"
		"\n"
		"Formát souboru:\n"
		"Vstupní data jsou uložena v textovém souboru. První řádek souboru je vyhrazen pro počet objektů v souboru a má formát:\n"
		"-count=N\n"
		"kde číslo je počet objektů v souboru. Následuje na každém řádku definice jednoho objektu.\n"
		"Počet řádků souboru odpovídá minimálně počtu objektů + 1 (první řádek).\n"
		"Další řádky souboru ignorujte. Řádek definující objekt je formátu:\n"
		"-OBJID X Y\n"
		"kde OBJID je v rámci souboru jednoznačný celočíselný identifikátor, X a Y jsou souřadnice objektu také celá čísla.\n"
		"Platí 0 <= X <= 1000, 0 <= Y <= 1000.";
// Macimální délka řádky v souboru.
const int line_l = 500;
// Maximální délka errorového textu k výpisu.
const int err_l = 1000;

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/**
 * Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 * Ukazatel NULL u pole objektu znamena kapacitu 0.
 *
 * @param *c Shluk který chceme inicializovat.
 * @param cap Počet objektů které má obsahovat.
 */
void init_cluster(struct cluster_t *c, int cap)
{
	assert(c != NULL);
    	assert(cap >= 0);

	c->size = 0;
	if (cap > 0)
	{
		// Alokace paměti pro objekty ve shluku;
		if ((c->obj = malloc(cap * sizeof(struct obj_t))) != NULL)
		{
			c->capacity = cap;
			return;
		}
	}

	c->capacity = 0;
	c->obj = NULL;
}

/**
 * Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 *
 * @param *c Shluk který chceme vyčistit.
 */
void clear_cluster(struct cluster_t *c)
{
	// Když v clusteru jsou objekty, tak je uvolní.
	if (c->capacity != 0)
	{
		free(c->obj);
	}

	init_cluster(c, 0);	
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/**
 * Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 *
 * @param *c Shluk kterému chceme změnit kapacitu.
 * @param new_cap Kapacitu na kterou zvětšit shluk.
 * @return Když zadaná nová kapacita je rovna nebo menší než aktuální,
 * 	   tak se vrátí ukazatel na shluk a nic se neprovede.
 *	   Když funkce proběhne správně, vrátí se ukazatel na zvětšený shluk.
 * 	   Když se nepovede realloc vrátí se NULL.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
    	// TUTO FUNKCI NEMENTE
    	assert(c);
    	assert(c->capacity >= 0);
    	assert(new_cap >= 0);

    	if (c->capacity >= new_cap)
        	return c;

    	size_t size = sizeof(struct obj_t) * new_cap;

    	void *arr = realloc(c->obj, size);
    	if (arr == NULL)
        	return NULL;

    	c->obj = (struct obj_t*)arr;
    	c->capacity = new_cap;
	return c;
}

/**
 * Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 * nevejde.
 *
 * @param *c Shluk do kterého chceme přidat objekt.
 * @param obj Objekt který chceme přidat do shluku.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
	// Pokud je cluster plný, tak se rozšíří.
	if (c->size == c->capacity)
	{
		if (resize_cluster(c, c->capacity + CLUSTER_CHUNK) == NULL)
		{
			return;
		}
	}

	c->obj[c->size] = obj;
        c->size++;
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);

/**
 * Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 * Objekty ve shluku 'c1' budou serazeny vzestupne podle identifikacniho cisla.
 * Shluk 'c2' bude nezmenen.
 *
 * @param *c1 Shluk do kterého se bude přidávat.
 * @param *c2 Shluk který se kopíruje.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
   	assert(c1 != NULL);
   	assert(c2 != NULL);

	// Dá objekty z c2 do c1.
	for (int i = 0; i < c2->size; i++)
	{
		append_cluster(c1, c2->obj[i]);
	}

	// Seřadí c1.
	sort_cluster(c1);
}

/**********************************************************************/
/* Prace s polem shluku */

/**
 * Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 * (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 * pocet shluku v poli.
 *
 * @param *carr Ukazatel na pole všech zhluků ze kterúho chceme shluk odstranit.
 * @param narr Velikost pole všech shluků.
 * @param idx Pozice shluku který chceme odstranit v poli shluků.
 * @return Počet prvků v poli shluků po odstranění shluku.
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
	assert(idx < narr);
    	assert(narr > 0);

	// Odstranění clusteru.
	// Velikost je teď narr - 1
	clear_cluster(&carr[idx]);

	// Posunutí položek za clusterem směrem dolů, aby tam nebylo volné místo.
	for (int i = idx; i < narr - 1; i++)
	{
		carr[i] = carr[i + 1];
	}
	
	return narr - 1;
}

/**
 * Pocita Euklidovskou vzdalenost mezi dvema objekty.
 * Výpočet je proveden pyhhagorovou větou.
 *
 * @param *o1 První objekt pro výpočet vzdálenosti.
 * @param *o2 Druhý objekt pro výpočet vzdálenosti.
 * @return Vypočítaná vzdálenost objektů.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
    	assert(o1 != NULL);
    	assert(o2 != NULL);

	// Výpočet vzdálenosti dvou objektů v dvourozměrném poly podle pythagorovy věty.
	return sqrtf(powf(o1->x - o2->x, 2.0) + pow(o1->y - o2->y, 2.0));
}

/**
 * Pocita vzdalenost dvou shluku.
 * Vypočítá vzdálenosti mezi všemi objekty těchto shluků a vybere tu nejměnší,
 * jako vzdálenost shluků (single linkage)
 *
 * @param *c1 První shluk pro výpočet vzdálenosti.
 * @param *c2 Druhý shluk pro výpočet vzdálenosti.
 * @return Vypočítaná vzdálenost clusterů.
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
    	assert(c1 != NULL);
    	assert(c1->size > 0);
    	assert(c2 != NULL);
    	assert(c2->size > 0);

	float cluster_distance = obj_distance(&c1->obj[0], &c2->obj[0]);
	float object_distance;

	// Vypočítání vzdáleností mezi všemi body z obou clusterů, výsledná vzdálenost je ta nejmenší dle zadání.
	for (int i = 0; i < c1->size; i++)
	{
		for (int j = 0; j < c2->size; j++)
		{
			object_distance = obj_distance(&c1->obj[i], &c2->obj[j]);
		}

		if (object_distance < cluster_distance)
		{
			cluster_distance = object_distance;
		}
	}

	return cluster_distance;
}

/**
 * Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 * hleda dva nejblizsi shluky. Nalezene shluky identifikuje jejich indexy v poli
 * 'carr'. Funkce nalezene shluky (indexy do pole 'carr') uklada do pameti na
 * adresu 'c1' resp. 'c2'.
 *
 * @param *carr Ukazatel na pole všech zhluků ve kterém se bude hledat.
 * @param narr Velikost pole všech shluků.
 * @param *c1 Index nalezeného prvního prvku z nejbližších v poli.
 * @param *c2 Index nalezeného druhého prvku z nejbližších v poli.
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
    	assert(narr > 0);

	float min_distance = cluster_distance(&carr[0], &carr[1]);
	float distance;

	//Kdyby ten co jsme si uložili do min distance byl nejmenší.
	*c1 = 0;
	*c2 = 1;

	// Porovná vzdálenosti všech clusterů v poli a vráti dva co si jsou nejbližší.
	for (int i = 0; i < narr; i++)
	{
		for (int j = i + 1; j < narr; j++)
		{
			distance = cluster_distance(&carr[i], &carr[j]);

			if (distance < min_distance)
			{
				min_distance = distance;
				*c1 = i;
				*c2 = j;
			}
		}
	}
}

/**
 * Pomocna funkce pro razeni shluku.
 * Řazení ID objektu podle quicksort.
 *
 * @param *a První objekt pro porovnání.
 * @param *b Druhí objekt pro porovnání.
 * @return -1 Když první objekt má menší ID.
 * 	    1 Když druhý objekt má menší ID. 
 * 	    0 Když mají stejní ID.
 */
static int obj_sort_compar(const void *a, const void *b)
{
	// TUTO FUNKCI NEMENTE
    	const struct obj_t *o1 = (const struct obj_t *)a;
    	const struct obj_t *o2 = (const struct obj_t *)b;
    	if (o1->id < o2->id) return -1;
    	if (o1->id > o2->id) return 1;
    	return 0;
}

/**
 * Razeni objektu ve shluku vzestupne podle jejich identifikatoru,
 * pomocí algoritmu quicksort.
 *
 * @param *c Cluster pro seřazení.
 */
void sort_cluster(struct cluster_t *c)
{
    	// TUTO FUNKCI NEMENTE
    	qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/**
 * Tisk shluku 'c' na stdout.
 *
 * @param *c Shluk který se má vytisknout.
 */
void print_cluster(struct cluster_t *c)
{
    	// TUTO FUNKCI NEMENTE
    	for (int i = 0; i < c->size; i++)
    	{
        	if (i) putchar(' ');
        	printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
    	}
    	putchar('\n');
}

/**
 * Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 * jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 * polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 * kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 * V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
 *
 * @param *filename Název souboru ze kterého se mají načítat objekty.
 * @param **arr Ukazatel na ukazatel na pole se shluky který alokujeme.
 * @return Počet načtených objektů (shluků) ze souboru.
 * 	   Když nastastane chyba uloží tak kam se odkazuje arr NULL.
 */
int load_clusters(char *filename, struct cluster_t **arr)
{
    	assert(arr != NULL);

	char line[line_l];
	int n;

	// Otevření souboru a ověrění otevření.
	FILE *data;
        if ((data = fopen(filename, "r")) == NULL)
        {
                return 0;
        }
	
	fgets(line, sizeof(line), data);

	// Odstranění \n z konce řádku.
	line[strlen(line) - 1] = '\0';

	// Načtení počtu objektů.
	n = strtol(&line[6], NULL, 10);

	// Inicializace clusterů.
	if ((*arr = malloc(n * sizeof(struct cluster_t))) == NULL)
	{
		fclose(data);
		return 0;
	}

	for (int i = 0; i < n; i++)
	{
		// Pomocná proměnná pro zapsání dat.
		struct obj_t tmp;
		int id;
		float x;
		float y;
	
		fgets(line, sizeof(line), data);

		// Odstranění \n z konce řádku.
		line[strlen(line) - 1] = '\0';

		init_cluster(&(*arr)[i], 0);

		// Hodnoty pro uložení od objektu.
		sscanf(line,"%d %f %f", &id, &x , &y);
		tmp.id = id;
		tmp.x = x;
		tmp.y = y;

		// Přidání objektu s daty do vytvoženího clusteru.
		append_cluster(&(*arr)[i], tmp);

		// Kontrola jestli se povedl append
		if ((&(*arr)[i])->size != 1)
		{
			fclose(data);
			
			// Vyčištění clusterů
			for (int i = 0; i < n; i++)
        		{
                		clear_cluster(arr[i]);
        		}
        		free(arr);
			*arr = NULL;
			return 0;
		}
	}

	// Uzavření souboru
	if (fclose(data) == EOF)
        {
		return 0;
        }

	return n;
}

/**
 * Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 * Tiskne se prvnich 'narr' shluku.
 *
 * @param *carr Ukazatel na pole shluků které se má tisknout.
 * @param narr Počet shluků který se má tisknout.
 */
void print_clusters(struct cluster_t *carr, int narr)
{
    	printf("Clusters:\n");
    	for (int i = 0; i < narr; i++)
    	{
        	printf("cluster %d: ", i);
        	print_cluster(&carr[i]);
    	}
}

/*
 * Spojování nejbližších shluků do té doby než je velikost pole shluků rovna argumentu N programu.
 * 
 * @param *clusters Ukazatel na pole ve kterém jsou uloženy všechny clustery.
 * @param clusters_count Počet clusterů v poli.
 * @param clusters_required Cílový počet clusterů který chceme.
 * @param *len_at_err Počet clusterů v poli když došlo k chybě, když k chybě nedojde tak se sem nic nezaipíše.
 * @return 1 Když funkce proběhne správně.
 * 	   0 když nastane chyba.
 */
int make_required_size(struct cluster_t *clusters, int clusters_count, int clusters_required, int *len_at_err)
{
	// Indexy clusterů v poli.
	int c1;
	int c2;
	int c1_capacity = 0;

	while (clusters_required < clusters_count)
	{
		// Najití clusterů pro seskupení.
		find_neighbours(clusters, clusters_count, &c1, &c2);

		// Spojení najitých clusterů a ověření rozšíření clusteru když je potřeba ho rozšířit.
		if (clusters[c1].capacity == clusters[c1].size)
		{
			c1_capacity = clusters[c1].capacity;
		}

		merge_clusters(&clusters[c1], &clusters[c2]);

		if (clusters[c1].capacity == c1_capacity)
		{
			*len_at_err = clusters_count;
			return 0;
		}
		
		// Vymazání clusteru který se spojil se svým sousedem.
		remove_cluster(clusters, clusters_count, c2);
		clusters_count--;
	}
	
	return 1;
}

/*
 * Kontrola správnosti obsahu souboru s objekty, před načítáním objektů do paměti..
 *
 * @param *data Soubor pro kontrolu..
 * @param clusters_required Cílový požadovaný počet shluků zadaný argumentem N programu..
 * @return "OK" Když je formát souboru správný.
 * 	     Když je nalezena chyba vrátí se chybové hlášení.
 */
char *check_file(FILE *data, int clusters_required)
{
	char line[line_l];
	char compare[7] = "\0"; // Pro kontrolu count=.
	char n[10] = "\0"; // N z count=N.
	int n_n= 0; // N z count=N v int podobě.
	int i = 0;

	// Kontrola správnosti zapsání prvního řádku v souboru.
	if (!fgets(line, sizeof(line), data))
	{
		return "Špatný vnitřní formát souboru, neobsahuje žádný řádek.";
	}
	
	// Odstranění \n z konce řádku.
	line[strlen(line) - 1] = '\0';

	// Kontrola minimální velikosti.
	if (strlen(line) < 7)
	{
		return "Špatně zadáno count=N v souboru s daty.";
	}

	// Kontola jestli na začátku prvního řádku je count=.
	strncpy(compare, line, sizeof(compare) - 1);
	if (strcmp(compare, "count=") != 0)
	{
		return "Špatně zadáno count=N v souboru s daty, neobsahuje count=.";
	}

	// Kontrola N.
	strcpy(n, &line[6]);
	while (n[i] != '\0')
	{
		if (!isdigit(n[i]))
		{
			return "Špatně zadáno count=N v souboru s daty, N není celé nezáporné číslo.";
		}
		i++;
	}

	// Kontrola jestli je N 0.
	n_n = strtol(n, NULL, 10);
	if (n_n == 0)
	{
		return "Špatně zadáno count=N v souboru s daty, objektů nemůže být 0.";
	}

	// Kontrola počtu objektů s zadaným počtem zhluků který chceme vytisknout.
	// Maximální počet shluků k vytištění je počet objektů které máme.
	if (clusters_required > n_n)
	{
		return "Špatně zadaný argument N programu, nejde ze zadaných objektů udělat víc shluků než máme.";
	}

	// Pro kontrolu jestli nejsou nějaká stejná ID.
	int ids[n_n];

	for (i = 0; i < n_n; i++)
	{
		// TODO kontrola aby se číslo vlezlo do int.
		unsigned int j = 0;
		char id[10] = "\0"; // ID z OBJID X Y.
		char x[20] = "\0"; // X z OBJID X Y.
                char y[20] = "\0"; // Y z OBJID X Y.
		int id_n; // ID z OBJID X Y v INT podobě.
                float x_n; // X z OBJID X Y v float podobě.
                float y_n; // Y z OBJID X Y v float podobě.

		// Ošetření kdyby N bylo větší než počet zapsaných souborů.
		if (!fgets(line, sizeof(line), data))
		{
			return "V souboru je míň objektů než zadáno N.";
		}

		// Odstranění \n z konce řádku.
		line[strlen(line) - 1] = '\0';

		// Kontrola minimální velikosti.
		if (strlen(line) < 5)
		{
			return "Špatně zadáno OBJID X Y v souboru s daty.";
		}

		// Kontrola jestli jsou to jen celá nezáporná čísla.
		while (line[j] != '\0')
		{
        		if (!isdigit(line[j]) && line[j] != ' ')
        		{
                		return "Špatně zadáno OBJID X Y v souboru s daty, mužou to být jen celá nezáporná čísla.";
        		}
			j++;
		}

		// Kontrola správného formátu.
		if (line[0] == ' ')
		{
			return "Špatně zadáno OBJID X Y v souboru s daty.";
		}

		j = 1;

		//Přeskočení OBJID
		while (line[j] != ' ')
		{
			if (j == strlen(line) - 1)
                	{
                        	return "Špatně zadáno OBJID X Y v souboru s daty, neobsahuje X a Y.";
                	}
			j++;
		}

		// Kontrola jestli některé ID není dvakrát.
		strncpy(id, line, j);
		id_n = strtol(id, NULL, 10);
		for (int l = 0; l < i; l++)
		{
			if (id_n == ids[l])
			{
				return "Špatně zadáno OBJID X Y v souboru s daty, každý objekt musí mýt jiné OBJID.";
			}
		}
		ids[i] = id_n;

		if (j == strlen(line) - 1)
                {
                	return "Špatně zadáno OBJID X Y v souboru s daty, neobsahuje X a Y.";
              	}
		
		j++;

		if (line[j] == ' ')
        	{
        		return "Špatně zadáno OBJID X Y v souboru s daty, špatný formát X.";
        	}

		if (j == strlen(line) - 1)
                {
                        return "Špatně zadáno OBJID X Y v souboru s daty, neobsahuje Y.";
                }

		// Poskládání řetězce X.
		do
		{
			char p[2] = "\0";
			p[0] = line[j];
			p[1] = '\0';
			strcat(x, p);
			j++;

			// Kdyby jsme náhodou nenašly druhou mezeru a budume na konci textu.
			// Tak chybí Y.
			if (j == strlen(line) - 1)
			{
				return "Špatně zadáno OBJID X Y v souboru s daty, neobsahuje Y.";
			}
		} while (line[j] != ' ');

		j++;

		// Poskládání řetězce Y.
		do
        	{
			if (line[j] == ' ')
			{
				return "Špatně zadáno OBJID X Y v souboru s daty, špatný formát Y.";
			}

                	char p[2] = "\0";
                	p[0] = line[j];
                	p[1] = '\0'; 
                	strcat(y, p);
                	j++;
        	} while (j != strlen(line));	

		// Souřadnice musí být rozsahu 0 <= x =< 1000.
		x_n = strtof(x, NULL);
		y_n = strtof(y, NULL);
		if (x_n > 1000 || y_n > 1000)
		{
			return "Špatně zadáno OBJID X Y v souboru s daty, X a Y nesmí být vetší než 1000.";
		}
	}

	return "ok";
}

/*
 * Kontrola jestly byly programu zadány parametry které potřebuje,
 * a jestly byly zadány správně.
 *
 * @param argc Počet argumentů programu.
 * @param *argv Odkaz na pole atgumentů se kterými je spuštěný program.
 * @param *clusters_required Požadovaný počet shluků, když je zadáno argumentem N přepíše se hodnota, jinak defaultě 1.
 * @return "OK" Když funkce proběhla bez chyby.
 *  	   Když nastala chyba vrátí se chybové hlášení.
 */
char *check_arguments(int argc, char *argv[], int *clusters_required)
{
	// Kontrola zadání správného počtu argumentů.
	if (argc != 3 && argc != 2)
	{
		return help;
	}

	if (argc == 3)
	{
		// Kontrola argumentu [N].
		int i = 0;
       		while (argv[2][i] != '\0')
        	{
        		if (!isdigit(argv[2][i]))
                	{
               			return "Špatně zadaný argument [N], musí to být celé nezáporné číslo.";
                	}

                	i++;
        	}

		if ((*clusters_required = strtol(argv[2], NULL, 10)) <= 0)
                {
                        return "Špatně zadaný argument [N], musí to být celé nezáporné číslo.";
                }
	}

	return "ok";
}

/**
 * Hlavní funkce ze které se volají všechny ostatní, tak aby se došlo k požadovanému výsledku spojení shluků.
 * 1. Kontrola argumentů.
 * 2. Kontrola obsahu souboru s objekty.
 * 3. Načtení objektů do pole shluků.
 * 4. Udělání požadováného počtu shluků.
 * 5. Vytisknutí shluků.
 * 6. Dealokace paměti které zabýrají shluky.
 *
 * @param argc Počet argumentů programu.
 * @param *argv	Odkaz na pole atgumentů se kterými je spuštěný program.
 * @return 0 Když program proběhl bez chyby.
 * 	   1 Když se někde vyskytla chyba při počítání výsledku.
 */
int main(int argc, char *argv[])
{
    	struct cluster_t *clusters; // Proměnná se všemi clustery.
	int clusters_required = 1; // Cílový počet shluků, defaultně 1.
	char err[err_l];
	int clusters_loaded; // Počet načtených clusterů.

	// Kontrola argumentů.	
	strcpy(err, check_arguments(argc, argv , &clusters_required));
	if (strcmp(err, "ok") != 0)
	{
		fprintf(stderr, "%s\n", err);
		return 1;
	}	

	// Otevření souboru a ověrění otevření.
	FILE *data;
        if ((data = fopen(argv[1], "r")) == NULL)
        {
                fprintf(stderr, "Nepodařilo se otevřít soubor s daty.\n");
                return 1;
        }

	// Kontrola obsahu souboru.
	strcpy(err, check_file(data, clusters_required));

	// Uzavření souboru
	if (fclose(data) == EOF)
        {
                fprintf(stderr, "Nepodařilo se uzavřít soubor s daty.\n");
		return 1;
        }

	if (strcmp(err, "ok") != 0)
	{
		fprintf(stderr, "%s\n", err);
		return 1;
	}

	// Načtení objektů do pole ze souboru.
	clusters_loaded = load_clusters(argv[1], &clusters);

	if(clusters == NULL)
	{
		fprintf(stderr, "Nepodařilo se načíst objekty.\n");
                return 1;
	}

	// Pro delalokaci clusterů když dojde k chybě.
	int len_at_err = 0;
	
	// Pospojování clusterů do požadované velikosti zadaní argumentem N programu.
	if (make_required_size(clusters, clusters_loaded, clusters_required, &len_at_err) == 0)
	{
		fprintf(stderr, "Nepodařilo se vytvořit cílový počet shluků.\n");

		// Dealokace clusterů v poli.
		for (int i = 0; i < len_at_err; i++)
		{
			clear_cluster(&clusters[i]);
		}
		free(clusters);

		return 1;
	}
	
	print_clusters(clusters, clusters_required);

	// Dealokace clusterů v poli.
	for (int i = 0; i < clusters_required; i++)
	{
		clear_cluster(&clusters[i]);
	}
	free(clusters);

	return 0;
}
