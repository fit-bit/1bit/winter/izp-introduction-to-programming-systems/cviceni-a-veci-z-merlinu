/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#include "struct.h"

/**
 * Inizializace objektu. Název objektu kopíruje. Objekt bude mít název
 * roven NULL, pokud se inicializace nezdařila.
 */
void object_ctor(Object *o, int id, char *name)
{
	o->name = malloc (sizeof(char) * strlen(name) + 1);
	if (o->name == NULL)
	{
		o->name = NULL;
	}
	else
	{
		o->id = id;
		strcpy(o->name,name);
	}
	/** TODO */
}

/**
 * Záměna dat dvou objektù.
 */
void object_swap(Object *i1, Object *i2)
{
	Object tmp = *i1;
	i1 = object_cpy(i1,i2);
	i2 = object_cpy(i2,&tmp);
	object_dtor(&tmp);
	/** TODO */
}

/**
 * Hluboká kopie objektu src na dst. Vrací dst, pokud se operace povedla,
 * jinak NULL.
 */
Object *object_cpy(Object *dst, Object *src)
{
	object_ctor(dst, src->id, src-> name);
	return dst;
	/*
	dst->id = src->id;
	dst->name = malloc (sizeof(char) * strlen(src->name) + 1);
	if (dst->name == NULL)
	{
		return NULL;
	}
		strcpy(dst->name,src->name);
		return dst;
	}
	*/

	/** TODO */
}

/**
 * Uvolní objekt (resp. jeho jméno) z paměti. 
 * Nastavi jmeno objektu na NULL.
 */
void object_dtor(Object *o)
{
	free(o->name);
	o->name = NULL;
	/** TODO */
}

/**
 * Vytiske hodnoty daneho objektu.
 */
void print_object(Object *o)
{
    printf("ID: %d, NAME: %s\n", o->id, o->name);
}
