#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

//funkce na sčítání čísel
int secti(int a, int b)
{
	return a + b;
}

int main(int argc, char *argv[])
{
	int i;
	/*
	char *s = "Hello";
	char c1 = '\0';
	char c2;
	//char *s2;
	//char c;
	
	i = 2;
	c2 = s[i];
	
	printf("Tisk i: %d\n", i);
	printf("Tisk c1: %d\n", c1);
	printf("Tisk c2: %d\n", c2);
	// výpisi znaků
	for( i = 0; i != (strlen(s) + 2); i++)
	{
		printf("Pozice %i v textu (char): %c\n", i, s[i]);
		printf("Pozice %i v textu (decimal): %d\n", i, s[i]);
	}

	// porovnání dvou řetězců
	if (argc > 1)
	{
		// porovnání obsahu ne ukayatelú;
		if (strcmp(argv[1], "--help") == 0) printf("Vypisuji napovedu\n");
		else printf("Argument není --help\n");	
	}
	else printf("Nezadán argument\n");

	// hledání argumentu -h
	for ( i = 1; i < argc ; i++)
	{
		if (strcmp(argv[i], "-h") == 0) printf("Argument -h je na pozici: %d\n", i);
	}
	*/
	// zapracování argumentů příkazové řádky
	bool hflag = false;
	bool oflag = false;
	//int cislo = 0;
	bool cislodef = false;
	bool chyba = true;

	for ( i = 1; i< argc ; i++ )
	{
		if (strcmp(argv[i], "-h") == 0)	
		{
			hflag = true;
			chyba = false;
		}
		if (strcmp(argv[i], "-o") == 0)
		{
			oflag = true;
			chyba = false;
		}
		if (atoi(argv[i]) != 0 )
		{
			cislodef = true;
			chyba = false;
		} 
	}
	if (hflag) printf("Zaznamenán argument -h\n");
	if (oflag) printf("Zaznamenán argument -o\n");
	if (cislodef) printf("Zaznamenáno cislo\n");
	if (chyba == true) printf("Nezadán argument který jsme chtěly\n");

	int a;
	int b;
	printf("Zadej a: ");
	scanf("%d", &a);
	printf("Zadej b: ");
	scanf("%d", &b);
	printf("Sečteno a+b: %d\n", secti(a,b));

	return 0;
}
