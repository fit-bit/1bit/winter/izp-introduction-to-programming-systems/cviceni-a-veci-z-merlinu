#include <stdio.h>
int main(int argc, char *argv[])
{
	// první část
	printf("Hello world!\n"); 
	
	// druhá část
	printf("This is number: %d\n",42); 

	int i;
	i = 42 + 3;
	printf("This is another number: %d\n",i);

	//část třetí
	if(i>44)
		printf("i is greater than 44\n");
	if(i>45)
		printf("i is greater than 45\n");
	else
		printf("i is NOT greater than 45\n");
	
	// část čtyři
	printf("Number of arguments: %d\n", argc);

	printf("0. argument: %s\n", argv[0]);
	printf("1. argument: %s\n", argv[1]);
	printf("2. argument: %s\n", argv[2]);
	printf("3. argument: %s\n", argv[3]);
	
	// část pět
	printf("0. argument: %s\n", argv[0]);
	if (argc > 1)
        	printf("1. argument: %s\n", argv[1]);
	if (argc > 2)
        	printf("2. argument: %s\n", argv[2]);
	if (argc > 3)
        	printf("3. argument: %s\n", argv[3]);
	
	// část šest
	i = 0;
	while (i < argc)
	{
		printf("%d. argument: %s\n", i, argv[i]);
		i = i + 1;
	}

	// bonus
	printf("for\n");
	for( i = 0; i < argc; i++ )
	{
		printf("%d. argument: %s\n", i, argv[i]);
	}

	//Načtení vstupu
	int promenna;
	scanf("%d", &promenna);
	printf("Hodnota je %d\n", promenna);
	
	float vzdalenost;
	scanf("%f", &vzdalenost);
	printf("Hodnota je %g\n", vzdalenost);

	char znak;
	scanf("%c", &znak);
	printf("Znak je %c\n", znak);

	char krestni[10];
	

	return 0;

}
