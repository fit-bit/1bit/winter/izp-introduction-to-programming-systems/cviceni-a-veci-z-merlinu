#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int main()
{
	unsigned int t = strtol("1000000009", NULL, 10);
	printf("%d\n", t);
	double y = 1;
	printf("%g\n", y);
	printf("%g\n", round(y));
	/*
	printf("pow: %.12g\n", pow(-0.1, 2));
	printf("pow: %.12g\n", pow(-0.1, 3));
	printf("pow: %.12g\n", pow(-0.1, 2.5));
	printf("pow: %.12g\n", pow(-0.1, -9.3));
	printf("pow: %.12g\n", pow(-0.1, -2));
	printf("pow: %.12g\n", pow(-3, -0.1));
	printf("pow: %.12g\n", pow(2, -t));
	printf("pow: %.12g\n", pow(0.1, INFINITY));
        printf("pow: %.12g\n", pow(2, INFINITY));
        printf("pow: %.12g\n", pow(INFINITY, 0.1));
        printf("pow: %.12g\n", pow(INFINITY, 2));
	printf("pow: %.12g\n", pow(INFINITY, -INFINITY));
	printf("pow: %.12g\n", pow(INFINITY, INFINITY));

	printf("pow: %.12g\n", pow(NAN, -NAN));
        printf("pow: %.12g\n", pow(-NAN, NAN));

	printf("pow: %.12g\n", pow(1, -NAN));
        printf("pow: %.12g\n", pow(1, NAN));
	
 	--pow -nan -nan 1
	--pow -nan -nan 2
	--pow -nan nan 1
	--pow -nan nan 2
	--pow nan -nan 1
	--pow nan -nan 2
	--pow nan nan 1
	--pow nan nan 2
 	*/
}
