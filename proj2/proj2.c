/******************************************************/
/* * *          Projekt 2: Iterační výpočty       * * */
/* * *                                            * * */
/* * *                  Verze:1                   * * */
/* * *                                            * * */
/* * *               Matěj Kudera                 * * */
/* * *              listopad 2018                 * * */
/******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

const int l_result = 500;
const double eps = 0.00000001;

/*
 * Prototypy funkcí.
 */
double taylor_log(double x, unsigned int n);
double cfrac_log(double x, unsigned int n);
double taylor_pow(double x, double y, unsigned int n);
double taylorcf_pow(double x, double y, unsigned int n);

double mylog(double x);
double mypow(double x, double y);

/*
 * Kontrola speciálnách případů exponenciální funkce.
 *
 * @param x: Hodnota ke kontrole.
 * @param y: Hodnota ke kontrole.
 * @return: Vrácení výsledku speciálního případu, když ne tak -1.
 */
double special_pow (double x, double y)
{
	// TODO u special nefunguje.
	if (x == 0.0 && y < 0.0)
	{
		return INFINITY;
	}
	if (x == 0.0 && y == 0.0)
        {
                return 1.0;
        }
	if (x == 0.0 && isnan(y))
        {
                return NAN;
        }
	if (x == 0.0 && y > 0.0)
        {
                return 0.0;
        }
	if (x == -INFINITY && y == -INFINITY)
        {
                return 0.0;
        }
	if (x == -INFINITY && isnan(y))
        {
                return NAN;
        }
	if (y == 0.0 || x == 1.0)
	{
		return 1.0;
	}
	if (x == -INFINITY && (y < 0.0 && y > -1.0))
        {
                return 0.0;
        }
	if (x == -INFINITY && y < 0)
        {
                return -1.0;
        }
	if (x < 0.0 && (round(y) != y))
        {
                return -NAN;
        }
	if (isnan(x) || isnan(y))
        {
                return NAN;
        }
	if ( x == INFINITY && y == -INFINITY)
	{
		return 0.0;
	}
	if ( x == INFINITY && y == INFINITY)
        {
                return INFINITY;
        }
	if ((x > 0 && x < 1) && y == -INFINITY)
	{
		return INFINITY;
	}
	if ((x >= 0 && x < 1) && y == INFINITY)
        {
                return 0.0;
        }
	if (x > 1 && y == -INFINITY)
        {
                return 0.0;
        }
	if (x > 1 && y == INFINITY)
        {
                return INFINITY;
        }
	if (x == INFINITY && y < 0)
	{
		return 0.0;
	}
	if (x == INFINITY)
        {
                return INFINITY;
        }

        return -1.0;
}


/*
 * Výpočet logaritmu pomocí Taylotova polynomu.
 *
 * @param x: Hodnota ze které se počítá logaritmus.
 * @param n: Počet iterací Taylorova polynomu.
 * @return: Vypočítaná hodnota logaritmu.
 */
double taylor_log(double x, unsigned int n)
{
	// Kontrola speciálních případů logaritmu.
	if (fabs(x) == 0.0)
        {
                return -INFINITY;
        }
        else if (x  < 0.0)
        {
                return NAN;
        }
        else if (x == INFINITY)
        {
                return INFINITY;
        }
	
	double t = 1.0;
        double s = 0.0;
	double y = 0.0;

	//Proměnná Taylorova polynomu pro číslo menší 1
	if (x < 1.0)
	{
		y = 1.0 - x;
	}
	//Proměnná Taylorova polynomu pro číslo větší než 1.
	else
	{
		y = (x - 1.0) / x;
	}

	// Výpočet
	for (double i = 1.0; i < n + 1; i++)
        {
		t *= y;
                s += t / i;
		if(isinf(s))
                {
                        break;
                }
        }

	if (x < 1.0)
	{
		return -s;
	}
	else
	{
		return s;
	}
}

/*
 * Výpočet logaritmu pomocí zřetězených zlomků.
 *
 * @param x: Hodnota ze které se počítá logaritmus.
 * @param n: Počet iterací zřetězeného zlomku.
 * @return: Vypočítaná hodnota logaritmu.
 */
double cfrac_log(double x, unsigned int n)
{
	// Kontrola speciálních případů logaritmu.
	if (fabs(x) == 0.0)
        {
                return -INFINITY;
        }
        else if (x  < 0.0)
        {
                return NAN;
        }
        else if (x == INFINITY)
        {
                return INFINITY;
        }

	// Výpočet
	double s = 0.0 ;
	x = (x - 1.0) / (1.0 + x);
	
	for (double i = n - 1; i > 0.0; i--)
	{
		s = ((i * i) * (x * x)) / ((2.0 * i + 1.0) - s);
		if(isinf(s))
                {
                        break;
                }
	}
	
	return (2.0 * x) / (1.0 - s);
}

/*
 * Výpočet exponenciální funkce pomocí Taylorova polynomu.
 * Přirozený logarirmus se ve funkci počítá funkcí taylor_log.
 *
 * @param x: Hodnota ze které se počítá exponenciální funkce.
 * @param y: Exponent exponenciální funkce.
 * @param n: Počet iterací Taylorova polynomu.
 * @return: Vypočítaná hodnota exponenciální funkce.
 */
double taylor_pow(double x, double y, unsigned int n)
{
	// Dle zadání Taylorova polynomu pro expenonciální funkci.
	if (x <= 0)
	{
		return NAN;
	}

	// Kontrola speciálních případů exponenciální funkce.
	double c;
        if ((c = special_pow(x,y)) != -1.0)
        {
                return c;
        }

	// Výpočet
	double t = 1.0;
        double s = t;
	double log = taylor_log(x,n);

	for (double i = 1.0; i < n; i++)
	{
		t = t * ((y * log) / i);
		s += t;
		if(isinf(s))
                {
                        break;
                }
	}

	return s;
}

/*
 * Výpočet exponenciální funkce pomocí Taylorova polynomu.
 * Přirozený logarirmus se ve funkci počítá funkcí cfrac_log.
 *
 * @param x: Hodnota ze které se počítá exponenciální funkce.
 * @param y: Exponent exponenciální funkce.
 * @param n: Počet iterací zřetězeního zlomku..
 * @return: Vypočítaná hodnota exponenciální funkce.
 */
double taylorcf_pow(double x, double y, unsigned int n)
{
	// Dle zadání Taylorova polynomu pro expenonciální funkci.
	if (x <= 0)
	{
		return NAN;
	}

	// Kontrola speciálních případů exponenciální funkce.
	double c;
        if ((c = special_pow(x,y)) != -1.0)
        {
                return c;
        }

	// Výpočet
	double t = 1.0;
        double s = t;
	double log = cfrac_log(x,n);

	for (double i = 1.0; i < n; i++)
	{
		t = t * ((y * log) / i);
		s += t;
		if(isinf(s))
                {
                        break;
                }
	}

	return s;
}

/*
 * Funkce mypow podle hodnoty argamentů vybere nejpřesnější typ výpočtu (taylor_pow nebo taylorcf_pow) a minimální počet iterací pro požadovanou přesnost.
 * Požadovaná přesnost je 8 významných číslic.
 *
 * @param x: Hodnota ze které se počítá exponenciální funkce.
 * @param y: Exponent exponenciální funkce.
 * @return: Vypočítaná hodnota exponenciální funkce.
 */
double mypow(double x, double y)
{
	// Kontrola speciálních případů exponenciální funkce.
	double c;
        if ((c = special_pow(x,y)) != -1.0)
        {
                return c;
        }

	// Pro záporné hodnoty X.
	int subzero = 0;
	if (x < 0)
	{
		if (round(y/2) != y/2)
		{
			subzero = 1;
		}
	}

	// Výpočet
	x = fabs(x);
	double t = 1.0;
	double s = t;
	double i = 1;
	double log = mylog(x);
	// Funkce my_log vybýrá lepší výpočet logaritmu pro celý polynom.
	do
	{
		t = t * ((y * log) / i);
		s += t;
		if(isinf(s))
		{
			break;
		}
		i++;
	}while(fabs(t) > eps);

	if (subzero == 1)
	{
		return -s;
	}
	else
	{
		return s;
	}

}


/*
 * Funkce mylog podle hodnoty argamentu vybere nejpřesnější typ výpočtu (Taylorův polynom nebo zřetězené zlomky) a minimální počet iterací pro požadovanou přesnost.
 * Požadovaná přesnost je 8 významných číslic.
 *
 * @param x: Hodnota ze které se počítá logaritmus.
 * @return: Vypočítaná hodnota logaritmu.
 */
double mylog(double x)
{
	// Kontrola speciálních případů logaritmu.
	if (fabs(x) == 0.0)
        {
                return -INFINITY;
        }
        else if (x  < 0.0)
        {
                return NAN;
        }
        else if (x == INFINITY)
        {
                return INFINITY;
        }

	double taylor_t = 1.0;
	double taylor_s = 0.0;
	double cfrac_last = 0.0;
	double cfrac = 0.0;
	double i = 1;
	// Počítání Taylorovým polinomem a zřetězenými zlomky. Ten který pude první přesnější na 8 platných míst, ten se vypíše.
	do
	{
		// Taylorův polynom pro číslo větší než 1.
		if (x < 1.0)
		{
			taylor_t *= 1.0 - x;
			taylor_s -= taylor_t / i;
		}
		// Taylorův polynom pro číslo větší než 1.
		else
		{
			taylor_t *= (x - 1.0) / x;
			taylor_s += taylor_t / i;
		}

		cfrac_last = cfrac;
		cfrac = cfrac_log(x, i);
		i++;
	} while(fabs(taylor_t) > eps && fabs(cfrac_last - cfrac) > eps);

	if (taylor_t <= eps)
	{
		return taylor_s;
	}
	else
	{
		return cfrac;
	}
}

/*
 * Kontrola jestli jsou argumenty programu zadány správně.
 *
 * @param argc: počet argumentů programu.
 * @param *argv: argumenty s kterými je zadaný program.
 * @return: log když se má počítat log.
 * 	    pow když se počítá pow.
 * 	    mylog když se počítá mylog,
 * 	    mypow když se počítá mypow.
 * 	    err string když je v argumentech chyba.
 */
char *check_arguments(int argc, char *argv[])
{
	// Vypsání help.
	if (argc == 1)
	{
		return "Program pro počátání hodnoty logaritmu a exponenciální funkce.\n"
		       "\n"
		       "Autor: Matěj Kudera\n"
		       "\n"
		       "Program pracuje s argumenty --log X N, --pow X Y N, --mylog X a --mypow X Y.";
	}

	// Kontrola volby --log.
	if (strcmp("--log", argv[1]) == 0)
	{
		if (argc != 4)
		{
			return "Špatně zadaný příkaz --log. Příkaz má vypadat --log X N.";
		}

		// Kontrola zadání speciálních případů. 
		if (strcmp("-NAN", argv[2]) != 0 && strcmp("-INFINITY", argv[2]) != 0 && strcmp("NAN", argv[2]) != 0 && strcmp("INFINITY", argv[2]) != 0 && strcmp("-INF", argv[2]) != 0 && strcmp("INF", argv[2]) != 0)
		{
			// Kontrola jestli argument je čísl0.
			int i = 0;
       			while (argv[2][i] != '\0')
        		{
                		if(!isdigit(argv[2][i]) && argv[2][i] != '-' && argv[2][i] != '.')
                		{
                       			return "Špatně zadaný příkaz --log. Argument X musí být číslo.";
                		}
                		i++;
        		}
		}

		// Argument N musí být celé nezáporné číslo.
		int i = 0;
		while (argv[3][i] != '\0')
		{
			if(!isdigit(argv[3][i]))
                        {
                        	return "Špatně zadaný příkaz --log. Argument N musí být celé nezáporné číslo.";
                        }
                      	i++;
		}

        		{
                		if(!isdigit(argv[2][i]) && argv[2][i] != '-' && argv[2][i] != '.')
                		{
                       			return "Špatně zadaný příkaz --log. Argument X musí být číslo.";
                		}
                		i++;
        		}
		}

		// Argument N musí být celé nezáporné číslo.
		int i = 0;
		while (argv[3][i] != '\0')
		{
			if(!isdigit(argv[3][i]))
                        {
                        	return "Špatně zadaný příkaz --log. Argument N musí být celé nezáporné číslo.";
                        }
                      	i++;
		}

		if (strtol(argv[3], NULL, 10) <= 0)
		{
			return "Špatně zadaný příkaz --log. Argument N musí být celé nezáporné číslo.";
		}

		// Kontrola velikosti čísel aby se vešly do svích datových typů.
		if (argv[2][0] == '-')
		{
			if (strlen(argv[2]) > 39)
			{
				return "Špatně zadaný příkaz --log. Argument X je moc levký pro datový typ programu.";
			}
		}
		else
		{
			if (strlen(argv[2]) > 38)
                        {
                                return "Špatně zadaný příkaz --log. Argument X je moc levký pro datový typ programu.";
                        }
		}

		if (strlen(argv[3]) > 10)
		{
			return "Špatně zadaný příkaz --log. Argument N je moc levký pro datový typ programu.";	
		}
		
		return "log";
}

/*
 * Kontrola argumentů pro počítání taylor_pow a cfrac_pow.
 *
 * @param argc: počet argumentů programu.
 * @param *argv: argumenty s kterými je zadaný program.
 * @return: Když vše správně, tak pow. Jinak chybové hlášení.
 */
char *check_pow(int argc, char *argv[])
{
	if (argc != 5)
		{
			return "Špatně zadaný příkaz --pow. Příkaz má vapadat --pow X Y N.";
		}
		
		for (int j = 2; j < 4; j++)
		{
			// Kontrola zadání speciálních případů.
			if (strcmp("-NAN", argv[j]) != 0 && strcmp("-INFINITY", argv[j]) != 0 && strcmp("NAN", argv[j]) != 0 && strcmp("INFINITY", argv[j]) != 0 && strcmp("-INF", argv[j]) != 0 && strcmp("INF", argv[j]) != 0)
			{
				// Kontrola jestli argumenty jsou jen čísla.
				int i = 0;
        			while (argv[j][i] != '\0')
        			{
                			if(!isdigit(argv[j][i]) && argv[j][i] != '-' && argv[j][i] != '.')
                			{
                        			return "Špatně zadaný příkaz --pow. Argumenty X a Y musí být čísla.";
                			}
                			i++;
        			}
			}
		}

		// Argument N musí být celé nezáporné číslo.
		int i = 0;
                while (argv[4][i] != '\0')
                {
                        if(!isdigit(argv[4][i]))
                        {
                                return "Špatně zadaný příkaz --pow. Argument N musí být celé nezáporné číslo.";
                        }
                        i++;
                }

		if (strtol(argv[4], NULL, 10) <= 0)
                {
                        return "Špatně zadaný příkaz --log. Argument N musí být celé nezáporné číslo.";
                }

		// Kontrola velikosti čísel aby se vešly do svích datových typů.
		for (int j = 2; j < 4; j++)
                {
			if (argv[j][0] == '-')
                	{
                        	if (strlen(argv[j]) > 39)
                        	{
                                	return "Špatně zadaný příkaz --pow. Argument X je moc levký pro datový typ programu.";
                        	}
                	}
                	else
                	{
                        	if (strlen(argv[j]) > 38)
                        	{
                                	return "Špatně zadaný příkaz --pow. Argument X je moc levký pro datový typ programu.";
                        	}
                	}
		}

		if (strlen(argv[4]) > 10)
		{
			return "Špatně zadaný příkaz --pow. Argument N je moc levký pro datový typ programu.";	
		}

		return "pow";
}

/*
 * Kontrola argumentů pro počítání mylog.
 *
 * @param argc: počet argumentů programu.
 * @param *argv: argumenty s kterými je zadaný program.
 * @return: Když vše správně, tak mylog. Jinak chybové hlášení.
 */
char *check_mylog(int argc, char *argv[])
{
	if (argc != 3)
		{
			return "Špatně zadaný příkaz --mylog. Příkaz má vypadat --mylog X.";
		}

		// Kontrola zadání speciálních případů.
		if (strcmp("-NAN", argv[2]) != 0 && strcmp("-INFINITY", argv[2]) != 0 && strcmp("NAN", argv[2]) != 0 && strcmp("INFINITY", argv[2]) != 0 && strcmp("-INF", argv[2]) != 0 && strcmp("INF", argv[2]) != 0)
                {
			// Kontrola jestli argument je jen číslo.
			int i = 0;
        		while (argv[2][i] != '\0')
        		{
                		if(!isdigit(argv[2][i]) && argv[2][i] != '-' && argv[2][i] != '.')
                		{
                        		return "Špatně zadaný příkaz --mylog. Argument X musí být číslo.";
                		}

                		i++;
        		}
		}

		// Kontrola velikosti čísel aby se vešly do svích datových typů.
		if (argv[2][0] == '-')
		{
			if (strlen(argv[2]) > 39)
			{
				return "Špatně zadaný příkaz --mylog. Argument X je moc levký pro datový typ programu.";
			}
		}
		else
		{
			if (strlen(argv[2]) > 38)
                        {
                                return "Špatně zadaný příkaz --mylog. Argument X je moc levký pro datový typ programu.";
                        }
		}

		return "mylog";
}

/*
 * Kontrola argumentů pro počítání mypow.
 *
 * @param argc: počet argumentů programu.
 * @param *argv: argumenty s kterými je zadaný program.
 * @return: Když vše správně, tak mypow. Jinak chybové hlášení.
 */
char *check_mypow(int argc, char *argv[])
{
	if (argc != 4)
                {
                        return "Špatně zadaný příkaz --mypow. Příkaz má vypadat --mypow X Y.";
                }

		for (int j = 2; j < 4; j++)
		{
			// Kontrola zadání speciálních případů.
			if (strcmp("-NAN", argv[j]) != 0 && strcmp("-INFINITY", argv[j]) != 0 && strcmp("NAN", argv[j]) != 0 && strcmp("INFINITY", argv[j]) != 0 && strcmp("-INF", argv[j]) != 0 && strcmp("INF", argv[j]) != 0)
                        {
				// Kontrola jestli argumenty jsou jen čísla.
				int i = 0;
        			while (argv[j][i] != '\0')
        			{
                			if(!isdigit(argv[j][i]) && argv[j][i] != '-' && argv[j][i] != '.')
                			{
                        			return "Špatně zadaný příkaz --mypow. Argumenty X a Y musí být čísla.";
                			}
                			i++;
        	
				}
			}
		}

		// Kontrola velikosti čísel aby se vešli do svích datových typů.
		for (int j = 2; j < 4; j++)
                {
			if (argv[j][0] == '-')
                	{
                        	if (strlen(argv[j]) > 39)
                        	{
                                	return "Špatně zadaný příkaz --mypow. Argument X je moc levký pro datový typ programu.";
                        	}
                	}
                printf("  taylor_pow(%g,%g) = %.12g\n", strtof(argv[2], NULL), strtof(argv[3], NULL), taylor_pow(strtof(argv[2], NULL), strtof(argv[3], NULL), strtol(argv[4], NULL, 10)));
                printf("taylorcf_pow(%g,%g) = %.12g\n", strtof(argv[2], NULL), strtof(argv[3], NULL), taylorcf_pow(strtof(argv[2], NULL), strtof(argv[3], NULL), strtol(argv[4], NULL, 10)));
	}
	else if(strcmp("mylog", result) == 0)
	{
		printf("   log(%g) = %.7e\n", strtof(argv[2], NULL), log(strtof(argv[2], NULL)));
                printf("my_log(%g) = %.7e\n", strtof(argv[2], NULL), mylog(strtof(argv[2], NULL)));
	}
	else if(strcmp("mypow", result) == 0)
	{
		printf("  pow(%g,%g) = %.7e\n", strtof(argv[2], NULL), strtof(argv[3], NULL), pow(strtof(argv[2], NULL), strtof(argv[3], NULL)));
                printf("mypow(%g,%g) = %.7e\n", strtof(argv[2], NULL), strtof(argv[3], NULL), mypow(strtof(argv[2], NULL), strtof(argv[3], NULL)));
	}
	else
	{
		fprintf(stderr, "%s\n",result);
	}

	return 0;
}
