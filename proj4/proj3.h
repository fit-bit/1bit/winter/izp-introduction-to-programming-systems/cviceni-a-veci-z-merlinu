/**
 * @mainpage Projekt 3
 * @link
 * proj3.h
 * @endlink
 *
 * @file proj3.h
 * @brief Kostra hlavickoveho souboru 3. projekt IZP 2018/19
 * 	  a pro dokumentaci Javadoc.
 * @author Matěj Kudera
 * @date Procinec 2018
 * @version 1.0
 */

/**
 * @brief Struktura objektu: identifikator a souradnice.
 */
struct obj_t {
    int id;
    float x;
    float y;
};

/**
 * @brief Shluk objektu:
 *	pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *      misto v poli),
 * 	ukazatel na pole shluku.
 */
struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/// Help programu.
extern char *help;
/// Maximální délka řádky v souboru.
extern const int line_l;
/// Maximální délka errorového textu k výpisu.
extern const int err_l;

/**
 * @defgroup Cluster Funkce pro práci se shluky.
 * @{
 */

/**
 * Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 * Ukazatel NULL u pole objektu znamena kapacitu 0.
 *
 * @post
 * Bude nastavená velikost a kapacita shluku a mallokem naalokovaný
 * prostor pro objekty. Když nastane chyba ukazatel na objekty bude NULL.
 *
 * @param *c Shluk který chceme inicializovat.
 * @param cap Počet objektů které má obsahovat.
 */ 
void init_cluster(struct cluster_t *c, int cap);

/**
 * Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 *
 * @post
 * Odalokují se objekty a pomocí funkce init_cluster se shluk nastavý
 * na prázdný.
 *
 * @param *c Shluk který chceme vyčistit.
 */
void clear_cluster(struct cluster_t *c);

/// Hodnota o kterou se vždy reallokuje cluster, když he plný.
extern const int CLUSTER_CHUNK;

/**
 * Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 *
 * @pre
 * Nová kapacita musí být větší nebo rovna 0.
 *
 * @post
 * Když se povede realloc shluk bude mít větší velikost o CLUSTER_CHUNK.
 *
 * @param *c Shluk kterému chceme změnit kapacitu.
 * @param new_cap Kapacitu na kterou zvětšit shluk.
 * @return Když zadaná nová kapacita je rovna nebo menší než aktuální,
 *         tak se vrátí ukazatel na shluk a nic se neprovede.
 *         Když funkce proběhne správně, vrátí se ukazatel na zvětšený shluk.
 *         Když se nepovede realloc vrátí se NULL.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap);

/**
 * Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 * nevejde.
 *
 * @post
 * Když nenastane chyba v resize_cluster, tak na konci objektů shluku bude objekt obj.
 *
 * @param *c Shluk do kterého chceme přidat objekt.
 * @param obj Objekt který chceme přidat do shluku.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj);

/**
 * Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 * Objekty ve shluku 'c1' budou serazeny vzestupne podle identifikacniho cisla.
 * Shluk 'c2' bude nezmenen.
 *
 * @post
 * Shluk c1 bude rozšířený o shluk c2 a bude seřazený vzestupně podle ID.
 *
 * @param *c1 Shluk do kterého se bude přidávat.
 * @param *c2 Shluk který se kopíruje.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2);

/**
 * Pomocna funkce pro razeni shluku.
 * Řazení ID objektu podle quicksort.
 *
 * @param *a První objekt pro porovnání.
 * @param *b Druhí objekt pro porovnání.
 * @return -1 Když první objekt má menší ID.
 *          1 Když druhý objekt má menší ID. 
 *          0 Když mají stejní ID.
 */
static int obj_sort_compar(const void *a, const void *b);

/**
 * Razeni objektu ve shluku vzestupne podle jejich identifikatoru,
 * pomocí algoritmu quicksort.
 *
 * @post
 * Objekty ve shluku budou seřazeny vzestupně podle ID.
 *
 * @param *c Cluster pro seřazení.
 */
void sort_cluster(struct cluster_t *c);

/**
 * Tisk shluku 'c' na stdout.
 *
 * @post
 * Všechny objekty ze shluku budou vytištěny na stdout.
 *
 * @param *c Shluk který se má vytisknout.
 */
void print_cluster(struct cluster_t *c);

/**
 * Pocita Euklidovskou vzdalenost mezi dvema objekty.
 * Výpočet je proveden pyhhagorovou větou.
 *
 * @param *o1 První objekt pro výpočet vzdálenosti.
 * @param *o2 Druhý objekt pro výpočet vzdálenosti.
 * @return Vypočítaná vzdálenost objektů.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2);

/**
 * Pocita vzdalenost dvou shluku.
 * Vypočítá vzdálenosti mezi všemi objekty těchto shluků a vybere tu nejměnší,
 * jako vzdálenost shluků (single linkage)
 *
 * @pre
 * Ve shluku c1 a c2 musí být víc objektů než 0.
 *
 * @param *c1 První shluk pro výpočet vzdálenosti.
 * @param *c2 Druhý shluk pro výpočet vzdálenosti.
 * @return Vypočítaná vzdálenost clusterů.
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2);

/**
 * @}
 */

/**
 * @defgroup Clusters Funkce pro praci s polem shluku.
 * @{
 */

/**
 * Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 * (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 * pocet shluku v poli.
 *
 * @pre
 * Velikost pole shluků narr musí být větší než 0.
 *
 * @post
 * Z pole shluků bude odstraněn shluk na pozici idx, když jsou za odstaněným shlukem
 * další prvkym, tak se všechny posunou o pozici níž. Výsledná velikost pole je narr - 1.
 *
 * @param *carr Ukazatel na pole všech zhluků ze kterúho chceme shluk odstranit.
 * @param narr Velikost pole všech shluků.
 * @param idx Pozice shluku který chceme odstranit v poli shluků.
 * @return Počet prvků v poli shluků po odstranění shluku.
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx);

/**
 * Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 * hleda dva nejblizsi shluky. Nalezene shluky identifikuje jejich indexy v poli
 * 'carr'. Funkce nalezene shluky (indexy do pole 'carr') uklada do pameti na
 * adresu 'c1' resp. 'c2'.
 *
 * @pre
 * Velikost pole shluků narr musí být větší než 0.
 *
 * @post
 * Indexy v poli shluků dvou nejbližších prvků budou uloženy do c1 a c2.
 *
 * @param *carr Ukazatel na pole všech zhluků ve kterém se bude hledat.
 * @param narr Velikost pole všech shluků.
 * @param *c1 Index nalezeného prvního prvku z nejbližších v poli.
 * @param *c2 Index nalezeného druhého prvku z nejbližších v poli.
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2);

/**
 * Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 * jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 * polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 * kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 * V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
 *
 * @post
 * Pro každý objekt ve vstupním souboru je vytvořen shluk s tím objektem a shluk
 * se uloží do pole všech shluků, které je tam kam ukazuje *arr.
 *
 * @param *filename Název souboru ze kterého se mají načítat objekty.
 * @param **arr Ukazatel na ukazatel na pole se shluky který alokujeme.
 * @return Počet načtených objektů (shluků) ze souboru.
 *         Když nastastane chyba uloží tak kam se odkazuje arr NULL.
 */
int load_clusters(char *filename, struct cluster_t **arr);

/**
 * Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 * Tiskne se prvnich 'narr' shluku.
 *
 * @post
 * Narr počet shluků pole všech shluků bude vytištěný na stdout pomocí print_cluster.
 *
 * @param *carr Ukazatel na pole shluků které se má tisknout.
 * @param narr Počet shluků který se má tisknout.
 */
void print_clusters(struct cluster_t *carr, int narr);

/**
 * Spojování nejbližších shluků do té doby než je velikost pole shluků rovna argumentu N programu.
 * 
 * @post
 * Když nenastane chyba pole všech shluků bude upraveno tak aby mělo velikost která je zadaná
 * argumentem N programu. Shluky které bylo nutno spojit pro dosažený požadované velikosti budou spojeny.
 *
 * @param *clusters Ukazatel na pole ve kterém jsou uloženy všechny clustery.
 * @param clusters_count Počet clusterů v poli.
 * @param clusters_required Cílový počet clusterů který chceme.
 * @param *len_at_err Počet clusterů v poli když došlo k chybě, když k chybě nedojde tak se sem nic nezaipíše.
 * @return 1 Když funkce proběhne správně.
 *         0 když nastane chyba.
 */
int make_required_size(struct cluster_t *clusters, int clusters_count, int clusters_required, int *len_at_err);

/**
 * @}
 */

/**
 * @defgroup Validation Funkce pro kontrolu správnosti souboru s objekty a argumentů programu.
 * @{
 */

/**
 * Kontrola správnosti obsahu souboru s objekty, před načítáním objektů do paměti..
 *
 * @post
 * Víme že v souboru s objekty není chyba.
 *
 * @param *data Soubor pro kontrolu..
 * @param clusters_required Cílový požadovaný počet shluků zadaný argumentem N programu..
 * @return "OK" Když je formát souboru správný.
 *         Když je nalezena chyba vrátí se chybové hlášení.
 */
char *check_file(FILE *data, int clusters_required);

/**
 * Kontrola jestly byly programu zadány parametry které potřebuje,
 * a jestly byly zadány správně.
 * 
 * @post
 * Víme že v argumentech programu není chyba.
 *
 * @param argc Počet argumentů programu.
 * @param *argv Odkaz na pole atgumentů se kterými je spuštěný program.
 * @param *clusters_required Požadovaný počet shluků, když je zadáno argumentem N přepíše se hodnota, jinak defaultě 1.
 * @return "OK" Když funkce proběhla bez chyby.
 *         Když nastala chyba vrátí se chybové hlášení.
 */
char *check_arguments(int argc, char *argv[], int *clusters_required);

/**
 * @}
 */
