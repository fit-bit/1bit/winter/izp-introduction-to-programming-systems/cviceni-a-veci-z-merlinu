#include <stdio.h>
#include <math.h>

double my_sqrt(double x, double eps)
{
	double y = x;
	double yp;
	int i = 0;
	do 
	{
		i++;
		yp = y;
		y = (0.5)*((x/y)+y);
		printf("krok: %d hodnota: %f\n",i,y);
	} while (fabs(yp - y) > eps);
	return y;
}

double my_sqr(double x, double eps)
{
	double t = 1;
	double s = t;
	int i = 0;
	while (fabs(t) > eps)
	{
		i++;
		t = (t*(x/i));
		s = s + t;
	}
	return s;
}

double my_log(double y, double eps)
{
	double t = 1 - y;
	double s = -t;
	int i = 0;
	while (fabs(t) > eps)
	{
		i++;
		t = (t*-(t/i));
		s = s + t;
	}
	return s;
}

int main()
{
	// výchozý hodnot
	double eps = 0.01;
	double x = 4.2;
	
	printf("druhá odmocnina z čésla: %.0f je: %f\n", x, my_sqrt(x,eps));

	// mocnina
	eps = 0.01;
	x = 4.2;

	printf("e na %f rovná se: %f\n",x,my_sqr(x,eps));

	// log
	// x = 1;
	// y = 10;
	//
	double y = 10.0;

	printf("log čísla %f je: %f", y, my_log(y,eps));	
	return 0;
}
